<?php declare(strict_types=1);

namespace Recommender\Item;

interface ItemRepositoryInterface
{
    public function getAllItems(): ItemCollection;
}
