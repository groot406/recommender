<?php declare(strict_types=1);

namespace Recommender\Item;

use Ramsey\Collection\Map\AbstractTypedMap;

class ItemCollection extends AbstractTypedMap
{
    public function getKeyType(): string
    {
        return 'string';
    }

    public function getValueType(): string
    {
        return 'string';
    }
}
