<?php declare(strict_types=1);

namespace Recommender\Evaluation;

use Recommender\Similarity\KnnSimilarity;
use Recommender\Similarity\SimilarityScoreInterface;
use Rubix\ML\Datasets\Dataset;
use Rubix\ML\Datasets\Unlabeled;

class EvaluationData
{
    /**
     * @var Dataset
     */
    private $fullTrainSet;

    /**
     * @var Dataset
     */
    private $fullAntiTestSet;

    /**
     * @var Dataset
     */
    private $trainSet;

    /**
     * @var Dataset
     */
    private $testSet;

    /**
     * @var array|null
     */
    private $userSamples;

    /**
     * @var array|null
     */
    private $leaveOneOutUserSamples;

    /**
     * @var Dataset
     */
    private $leaveOneOutTrainSet;

    /**
     * @var Dataset
     */
    private $leaveOneOutTestSet;

    /**
     * @var Dataset
     */
    private $leaveOneOutAntiTestSet;

    /**
     * @var float|int
     */
    private $globalMean;

    /**
     * @var KnnSimilarity
     */
    private $similarity;


    public function __construct(Dataset $dataset)
    {
        $dataset->randomize();
        $this->fullTrainSet = &$dataset;
        [$this->trainSet, $this->testSet] = $dataset->split(0.75);
    }

    public function getGlobalMean(): float
    {
        if ($this->globalMean === null) {
            $this->globalMean = array_sum($this->fullTrainSet->column(2)) / $this->fullTrainSet->numRows();
        }

        return $this->globalMean;
    }

    /**
     * @return Dataset
     */
    public function getFullTrainSet(): Dataset
    {
        return $this->fullTrainSet;
    }

    /**
     * @return Dataset
     */
    public function getTrainSet(): Dataset
    {
        return $this->trainSet;
    }

    /**
     * @return Dataset
     */
    public function getTestSet(): Dataset
    {
        return $this->testSet;
    }

    /**
     * @return Dataset
     */
    public function getFullAntiTestSet(): Dataset
    {
        if ($this->fullAntiTestSet === null) {
            $this->fullAntiTestSet = $this->buildAntiTestSetForFullTrainSet();
        }

        return $this->fullAntiTestSet;
    }

    public function getAntiTestSetForUser(string $targetUser): Dataset
    {
        $userItems = $this->getUserMapFromFullTrainSet()[$targetUser] ?? null;
        $allUniqueItems = array_unique($this->getFullTrainSet()->column(1));

        if ($userItems === null) {
            $notRatedItems = &$allUniqueItems;
        } else {
            $notRatedItems = array_diff($allUniqueItems, $userItems);
        }

        $samples = [];
        $globalMean = $this->getGlobalMean();
        foreach ($notRatedItems as $item) {
            $samples[] = [$targetUser, $item, $globalMean];
        }

        return new Unlabeled($samples, false);
    }

    /**
     * @return Dataset
     */
    public function getLeaveOneOutTrainSet(): Dataset
    {
        if ($this->leaveOneOutTrainSet === null) {
            $this->buildLeaveOneOutTestAndTrainSet();
        }

        return $this->leaveOneOutTrainSet;
    }

    /**
     * @return Dataset
     */
    public function getLeaveOneOutTestSet(): Dataset
    {
        if ($this->leaveOneOutTestSet === null) {
            $this->buildLeaveOneOutTestAndTrainSet();
        }

        return $this->leaveOneOutTestSet;
    }

    private function buildLeaveOneOutTestAndTrainSet(): void
    {
        $this->leaveOneOutTrainSet = clone $this->getFullTrainSet();
        $this->leaveOneOutTestSet = $this->leaveOneOutPerUser($this->leaveOneOutTrainSet);
    }

    /**
     * @return Dataset
     */
    public function getLeaveOneOutAntiTestSet(): Dataset
    {
        if ($this->leaveOneOutAntiTestSet === null) {
            $this->leaveOneOutAntiTestSet = $this->buildAntiTestSetForLeaveOneOutTrainSet();
        }

        return $this->leaveOneOutAntiTestSet;
    }

    public function getSimilarity(): SimilarityScoreInterface
    {
        if ($this->similarity === null) {
//          $knn = new KNearestNeighbors();
//          $knn->train($this->fullTrainSet);
            $this->similarity = new KnnSimilarity();
        }

        return $this->similarity;
    }

    private function buildAntiTestSetForFullTrainSet(): Dataset
    {
        $users = array_unique($this->getFullTrainSet()->column(0));
        $items = array_unique($this->getFullTrainSet()->column(1));

        $samples = [];
        $userMap = $this->getUserMapFromFullTrainSet();
        $globalMean = $this->getGlobalMean();
        foreach ($users as $user) {
            $userItems = $userMap[$user];
            $notRatedItems = array_diff($items, $userItems);
            foreach ($notRatedItems as $item) {
                $samples[] = [$user, $item, $globalMean];
            }
        }

        return new Unlabeled($samples, false);
    }

    private function leaveOneOutPerUser(Dataset $leaveOneOutTrainSet): Dataset
    {
        $testSetSampleOffsets = [];
        foreach ($leaveOneOutTrainSet as $offset => $sample) {
            $userId = $sample[0];
            if (isset($testSetSampleOffsets[$userId])) {
                continue;
            }

            $testSetSampleOffsets[$userId] = $offset;
        }

        arsort($testSetSampleOffsets);

        $testSetSamples = [];
        foreach ($testSetSampleOffsets as $userId => $offset) {
            $testSetSamples[] = $leaveOneOutTrainSet->sample($offset);
            $leaveOneOutTrainSet->dropRow($offset);
        }

        return new Unlabeled($testSetSamples, false);
    }

    private function buildAntiTestSetForLeaveOneOutTrainSet(): Dataset
    {
        $users = array_unique($this->getLeaveOneOutTrainSet()->column(0));
        $items = array_unique($this->getLeaveOneOutTrainSet()->column(1));

        $samples = [];
        $userMap = $this->getUserMapFromLeaveOneOutTrainSet();

        $globalMean = $this->getGlobalMean();
        foreach ($users as $user) {
            $userItems = $userMap[$user] ?? null;
            if ($userItems !== null) {
                $notRatedItems = array_diff($items, $userItems);
            } else {
                $notRatedItems = &$items;
            }

            foreach ($notRatedItems as $item) {
                $samples[] = [$user, $item, $globalMean];
            }
        }

        return new Unlabeled($samples, false);
    }

    /**
     * @return array
     */
    private function getUserMapFromFullTrainSet(): array
    {
        if (!$this->userSamples) {
            $this->userSamples = [];
            foreach ($this->getFullTrainSet() as $sample) {
                if (!array_key_exists($sample[0], $this->userSamples)) {
                    $this->userSamples[$sample[0]] = [];
                }
                $this->userSamples[$sample[0]][] = $sample[1];
            }
        }

        return $this->userSamples;
    }

    /**
     * @return array
     */
    private function getUserMapFromLeaveOneOutTrainSet(): array
    {
        if (!$this->leaveOneOutUserSamples) {
            $this->leaveOneOutUserSamples = [];
            foreach ($this->getLeaveOneOutTrainSet() as $sample) {
                if (!array_key_exists($sample[0], $this->leaveOneOutUserSamples)) {
                    $this->leaveOneOutUserSamples[$sample[0]] = [];
                }
                $this->leaveOneOutUserSamples[$sample[0]][] = $sample[1];
            }
        }

        return $this->leaveOneOutUserSamples;
    }
}
