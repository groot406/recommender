<?php declare(strict_types=1);

namespace Recommender\Evaluation;

use Ramsey\Collection\Map\AbstractTypedMap;

class EvaluationResultMap extends AbstractTypedMap
{
    public function getKeyType(): string
    {
        return 'string';
    }

    public function getValueType(): string
    {
        return EvaluationResult::class;
    }
}
