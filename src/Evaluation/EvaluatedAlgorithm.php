<?php declare(strict_types=1);

namespace Recommender\Evaluation;

use Recommender\Metrics\Accuracy;
use Recommender\Metrics\Coverage;
use Recommender\Metrics\Hitrate;
use Recommender\Metrics\Metrics;
use Recommender\Novelty\PopularityScoreInterface;
use Recommender\Novelty\RatingCountPopularity;
use Recommender\Prediction\Prediction;
use Recommender\Prediction\PredictionInterface;
use Recommender\Prediction\Predictions;
use Recommender\Recommender\RecommenderAlgorithm;
use Recommender\Similarity\SimilarityScoreInterface;
use Recommender\User\UserPredictionMap;
use Recommender\User\UserPredictionsMap;
use Rubix\ML\Datasets\Dataset;

class EvaluatedAlgorithm implements RecommenderAlgorithm
{
    private const ALL_METRICS = [
        Metrics::MAE,
        Metrics::RMSE,
        Metrics::USER_COVERAGE,
        Metrics::DIVERSITY,
        Metrics::NOVELTY,
        Metrics::HITRATE,
        Metrics::CUMULATIVE_HIT_RATE,
        Metrics::AVERAGE_RECIPROCAL_HIT_RANK,
        Metrics::RATING_HIT_RATE
    ];

    /**
     * @var RecommenderAlgorithm
     */
    private $algorithm;

    /**
     * @var Predictions|null
     */
    private $predictions;

    /**
     * @var int|null
     */
    private $numberOfUsers;

    /**
     * @var Predictions
     */
    private $leaveOneOutPredictions;

    /**
     * @var Predictions|null
     */
    private $allPredictions;

    public function __construct(RecommenderAlgorithm $algorithm)
    {
        $this->algorithm = $algorithm;
    }

    public function evaluate(EvaluationData $evaluationData, ?array $metrics = null, int $topN = 10): EvaluationResult
    {
        if ($metrics === null) {
            $metrics = self::ALL_METRICS;
        }

        return new EvaluationResult(
            in_array(Metrics::MAE, $metrics, true) ?
                $this->evaluateMAE($this->getPredictions($evaluationData)) : null,
            in_array(Metrics::RMSE, $metrics, true) ?
                $this->evaluateRMSE($this->getPredictions($evaluationData)) : null,
            in_array(Metrics::USER_COVERAGE, $metrics, true) ? $this->evaluateUserCoverage(
                $this->getPredictions($evaluationData),
                $this->getNumberOfUsers($evaluationData)
            ) : null,
            in_array(Metrics::DIVERSITY, $metrics, true) ? $this->evaluateDiversity(
                $this->getPredictions($evaluationData)->groupByUser()->topN($topN),
                $evaluationData->getSimilarity()
            ) : null,
            in_array(Metrics::NOVELTY, $metrics, true) ? $this->evaluateNovelty(
                $this->getPredictions($evaluationData)->groupByUser()->topN($topN),
                new RatingCountPopularity($evaluationData->getFullTrainSet())
            ) : null,
            in_array(Metrics::HITRATE, $metrics, true) ? $this->evaluateHitrate(
                $evaluationData,
                $topN
            ) : null,
            in_array(Metrics::CUMULATIVE_HIT_RATE, $metrics, true) ? $this->evaluateCumulativeHitrate(
                $evaluationData,
                $topN
            ) : null,
            in_array(Metrics::AVERAGE_RECIPROCAL_HIT_RANK, $metrics, true) ? $this->evaluateAverageReciprocalHitRank(
                $evaluationData,
                $topN
            ) : null,
            in_array(Metrics::RATING_HIT_RATE, $metrics, true) ? $this->evaluateRatingHitRate() : null,
        );
    }

    private function getPredictions(EvaluationData $evaluationData): Predictions
    {
        if ($this->predictions === null) {
            $this->algorithm->fit($evaluationData->getTrainSet());
            $this->predictions = $this->algorithm->test($evaluationData->getTestSet());
        }

        return $this->predictions;
    }

    private function getLeaveOneOutPredictions(EvaluationData $evaluationData): UserPredictionMap
    {
        if ($this->leaveOneOutPredictions === null) {
            $this->algorithm->fit($evaluationData->getLeaveOneOutTrainSet());
            $this->leaveOneOutPredictions = $this->algorithm->test($evaluationData->getLeaveOneOutTestSet());
            $this->allPredictions = $this->algorithm->test($evaluationData->getLeaveOneOutAntiTestSet());
        }

        $userPredictionMap = new UserPredictionMap();

        /** @var Prediction $leftOutPrediction */
        foreach ($this->leaveOneOutPredictions as $leftOutPrediction) {
            $userPredictionMap->put($leftOutPrediction->getUser(), $leftOutPrediction);
        }

        return $userPredictionMap;
    }

    private function getAllPredictions(EvaluationData $evaluationData): Predictions
    {
        if ($this->allPredictions === null) {
            $this->getLeaveOneOutPredictions($evaluationData);
        }

        if ($this->allPredictions === null) {
            return new Predictions();
        }

        return $this->allPredictions;
    }

    private function getNumberOfUsers(EvaluationData $evaluationData): int
    {
        if (!$this->numberOfUsers) {
            $this->numberOfUsers = count(array_unique($evaluationData->getFullTrainSet()->column(0)));
        }

        return $this->numberOfUsers;
    }

    public function evaluateMAE(Predictions $predictions): float
    {
        $accuracy = new Accuracy();

        return $accuracy->mae($predictions);
    }

    public function evaluateRMSE(Predictions $predictions): float
    {
        $accuracy = new Accuracy();

        return $accuracy->rmse($predictions);
    }

    public function evaluateUserCoverage(Predictions $predictions, int $numberOfUsers): float
    {
        $coverage = new Coverage();

        return $coverage->userCoverage($predictions, $numberOfUsers);
    }

    public function evaluateDiversity(UserPredictionsMap $predictions, SimilarityScoreInterface $similarity): float
    {
        $coverage = new Coverage();

        return $coverage->diversity($predictions, $similarity);
    }

    public function evaluateNovelty(UserPredictionsMap $predictions, PopularityScoreInterface $popularity): float
    {
        $coverage = new Coverage();

        return $coverage->novelty($predictions, $popularity);
    }

    public function evaluateHitrate(EvaluationData $evaluationData, int $topN): float
    {
        $hitRate = new Hitrate();

        return $hitRate->hitrate(
            $this->getAllPredictions($evaluationData)->groupByUser()->topN($topN),
            $this->getLeaveOneOutPredictions($evaluationData)
        );
    }

    public function evaluateCumulativeHitrate(EvaluationData $evaluationData, int $topN): float
    {
        $hitRate = new Hitrate();

        return $hitRate->cumulativeHitRate(
            $this->getAllPredictions($evaluationData)->groupByUser()->topN($topN),
            $this->getLeaveOneOutPredictions($evaluationData),
            4
        );
    }

    public function evaluateAverageReciprocalHitRank(EvaluationData $evaluationData, int $topN): float
    {
        $hitRate = new Hitrate();

        return $hitRate->averageReciprocalHitRank(
            $this->getAllPredictions($evaluationData)->groupByUser()->topN($topN),
            $this->getLeaveOneOutPredictions($evaluationData)
        );
    }

    public function evaluateRatingHitRate(): array
    {
        return [];
    }

    public function fit(Dataset $dataset): void
    {
        $this->algorithm->fit($dataset);
    }

    public function test(Dataset $dataset): Predictions
    {
        return $this->algorithm->test($dataset);
    }

    public function predict(string $userIdentifier, string $itemIdentifier, ?float $actual = null): PredictionInterface
    {
        return $this->algorithm->predict($userIdentifier, $itemIdentifier, $actual);
    }
}
