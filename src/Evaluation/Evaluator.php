<?php declare(strict_types=1);

namespace Recommender\Evaluation;

use Recommender\Recommender\RecommenderAlgorithm;
use Rubix\ML\Datasets\Dataset;

class Evaluator
{
    /**
     * @var EvaluationData
     */
    private $dataset;

    /**
     * @var array
     */
    private $algorithms = [];

    public function __construct(Dataset $dataset)
    {
        $this->dataset = new EvaluationData($dataset);
    }

    public function addAlgorithm(RecommenderAlgorithm $algorithm, string $name): void
    {
        $this->algorithms[$name] = new EvaluatedAlgorithm($algorithm);
    }

    public function evaluate(?array $metrics = null, int $topN = 10): EvaluationResultMap
    {
        $resultMap = new EvaluationResultMap();

        /** @var EvaluatedAlgorithm $algorithm */
        foreach ($this->algorithms as $name => $algorithm) {
            $resultMap[$name] = $algorithm->evaluate($this->dataset, $metrics, $topN);
        }

        return $resultMap;
    }

    public function getSamplePredictions(string $user, int $n): array
    {
        $algorithmPredictions = [];
        $testSet = $this->dataset->getAntiTestSetForUser($user);
        /** @var EvaluatedAlgorithm $algorithm */
        foreach ($this->algorithms as $name => $algorithm) {
            $algorithm->fit($this->dataset->getFullTrainSet());
            $predictions = $algorithm->test($testSet);
            $algorithmPredictions[$name] = $predictions->topN($n);
        }

        return $algorithmPredictions;
    }
}
