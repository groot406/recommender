<?php declare(strict_types=1);

namespace Recommender\Evaluation;

class ConsoleResultPrinter
{
    public function print(EvaluationResultMap $resultMap): void
    {
        $this->printRow(['Name', 'MAE', 'RMSE', 'HR', 'cHR', 'ARHR', 'Coverage', 'Diversity', 'Novelty']);
        $this->printRow([
            '====================',
            '==========',
            '==========',
            '==========',
            '==========',
            '==========',
            '==========',
            '==========',
            '=========='
        ]);
        /**
         * @var string $name
         * @var EvaluationResult $result
         */
        foreach ($resultMap as $name => $result) {
            $this->printRow([
                $name,
                $result->getMae(),
                $result->getRmse(),
                $result->getHitrate(),
                $result->getCumulativeHitRate(),
                $result->getAverageReciprocalHitRank(),
                $result->getUserCoverage(),
                $result->getDiversity(),
                $result->getNovelty()
            ]);
        }
    }

    private function printRow(array $data): void
    {
        echo str_pad((string)$data[0], 20);
        for ($i = 1, $iMax = count($data); $i < $iMax; $i++) {
            if (is_float($data[$i])) {
                echo str_pad((string)number_format($data[$i], 2), 10);
            } else {
                echo str_pad($data[$i]??'', 10);
            }
        }
        echo "\n";
    }
}
