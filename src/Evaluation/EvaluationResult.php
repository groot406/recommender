<?php declare(strict_types=1);

namespace Recommender\Evaluation;

class EvaluationResult
{
    /**
     * @var float|null
     */
    private $mae;

    /**
     * @var float|null
     */
    private $rmse;

    /**
     * @var float|null
     */
    private $userCoverage;

    /**
     * @var float|null
     */
    private $diversity;

    /**
     * @var float|null
     */
    private $novelty;

    /**
     * @var float|null
     */
    private $hitrate;

    /**
     * @var float|null
     */
    private $cumulativeHitRate;

    /**
     * @var float|null
     */
    private $averageReciprocalHitRank;

    /**
     * @var array|null
     */
    private $ratingHitRate;

    public function __construct(
        ?float $mae = null,
        ?float $rmse = null,
        ?float $userCoverage = null,
        ?float $diversity = null,
        ?float $novelty = null,
        ?float $hitrate = null,
        ?float $cumulativeHitRate = null,
        ?float $averageReciprocalHitRank = null,
        ?array $ratingHitRate = null
    ) {
        $this->mae = $mae;
        $this->rmse = $rmse;
        $this->userCoverage = $userCoverage;
        $this->diversity = $diversity;
        $this->novelty = $novelty;
        $this->hitrate = $hitrate;
        $this->cumulativeHitRate = $cumulativeHitRate;
        $this->averageReciprocalHitRank = $averageReciprocalHitRank;
        $this->ratingHitRate = $ratingHitRate;
    }

    /**
     * @return float|null
     */
    public function getMae(): ?float
    {
        return $this->mae;
    }

    /**
     * @return float|null
     */
    public function getRmse(): ?float
    {
        return $this->rmse;
    }

    /**
     * @return float|null
     */
    public function getUserCoverage(): ?float
    {
        return $this->userCoverage;
    }

    /**
     * @return float|null
     */
    public function getDiversity(): ?float
    {
        return $this->diversity;
    }

    /**
     * @return float|null
     */
    public function getNovelty(): ?float
    {
        return $this->novelty;
    }

    /**
     * @return float|null
     */
    public function getHitrate(): ?float
    {
        return $this->hitrate;
    }

    /**
     * @return float|null
     */
    public function getCumulativeHitRate(): ?float
    {
        return $this->cumulativeHitRate;
    }

    /**
     * @return float|null
     */
    public function getAverageReciprocalHitRank(): ?float
    {
        return $this->averageReciprocalHitRank;
    }

    /**
     * @return array|null
     */
    public function getRatingHitRate(): ?array
    {
        return $this->ratingHitRate;
    }
}
