<?php declare(strict_types=1);

namespace Recommender\User;

use Ramsey\Collection\Map\AbstractTypedMap;
use Recommender\Prediction\Predictions;

class UserPredictionsMap extends AbstractTypedMap
{
    /**
     * @var array
     */
    private $cache = [];

    public function getKeyType(): string
    {
        return 'mixed';
    }

    public function getValueType(): string
    {
        return Predictions::class;
    }

    public function topN(int $n): UserPredictionsMap
    {
        return $this->mapPrediction('topN', $n);
    }

    public function ratingAbove(float $threshold): UserPredictionsMap
    {
        return $this->mapPrediction('ratingAbove', $threshold);
    }

    /**
     * @param string $function
     * @param mixed ...$parameters
     * @return self
     */
    private function mapPrediction(string $function, ...$parameters): self
    {
        $hash = md5(var_export([$function, $parameters], true));
        if (isset($this->cache[$hash])) {
            return $this->cache[$hash];
        }

        /**
         * @var UserPredictionsMap
         */
        $filteredUserPredictionsMap = new self();

        /**
         * @var Predictions $predictions
         */
        foreach ($this as $userId => $predictions) {
            $filteredUserPredictionsMap[$userId] = $predictions->$function(...$parameters);
        }

        $this->cache[$hash] = $filteredUserPredictionsMap;

        return $filteredUserPredictionsMap;
    }
}
