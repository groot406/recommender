<?php declare(strict_types=1);

namespace Recommender\User;

use Ramsey\Collection\Map\AbstractTypedMap;
use Recommender\Prediction\PredictionInterface;

class UserPredictionMap extends AbstractTypedMap
{
    public function getKeyType(): string
    {
        return 'mixed';
    }

    public function getValueType(): string
    {
        return PredictionInterface::class;
    }

    public function ratingAbove(float $threshold): self
    {
        $filteredUserPredictionMap = new self();
        /**
         * @var PredictionInterface $prediction
         */
        foreach ($this as $userId => $prediction) {
            if ($prediction->getRating() >= $threshold) {
                $filteredUserPredictionMap[$userId] = $prediction;
            }
        }

        return $filteredUserPredictionMap;
    }
}
