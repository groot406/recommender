<?php declare(strict_types=1);

namespace Recommender\Prediction;

class Pair
{
    /**
     * @var Prediction
     */
    private $predictionA;

    /**
     * @var Prediction
     */
    private $predictionB;

    public function __construct(Prediction $predictionA, Prediction $predictionB)
    {
        $this->predictionA = $predictionA;
        $this->predictionB = $predictionB;
    }

    /**
     * @return Prediction
     */
    public function getPredictionA(): Prediction
    {
        return $this->predictionA;
    }

    /**
     * @return Prediction
     */
    public function getPredictionB(): Prediction
    {
        return $this->predictionB;
    }

    public function toArray(): array
    {
        return [$this->predictionA, $this->predictionB];
    }

    public function toPredictions(): Predictions
    {
        return new Predictions($this->toArray());
    }
}
