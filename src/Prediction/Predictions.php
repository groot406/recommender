<?php declare(strict_types=1);

namespace Recommender\Prediction;

use Ramsey\Collection\AbstractCollection;
use Recommender\User\UserPredictionsMap;

class Predictions extends AbstractCollection
{
    /**
     * @var UserPredictionsMap|null
     */
    private $userMap;

    public function getType(): string
    {
        return PredictionInterface::class;
    }

    public function groupByUser(): UserPredictionsMap
    {
        if ($this->userMap === null) {
            $this->userMap = new UserPredictionsMap();

            /** @var PredictionInterface $prediction */
            foreach ($this as $prediction) {
                if (!array_key_exists($prediction->getUser(), $this->userMap->toArray())) {
                    $this->userMap[$prediction->getUser()] = new self();
                }
                $this->userMap[$prediction->getUser()]->add($prediction);
            }
        }

        return $this->userMap;
    }

    public function ratingAbove(float $threshold): self
    {
        return new self(
            $this->filter(static function (PredictionInterface $prediction) use ($threshold) {
                return $prediction->getRating() >= $threshold;
            })->toArray()
        );
    }

    public function topN(int $n): self
    {
        $epsilon = 0.00001;

        uasort($this->data, static function (Prediction $a, Prediction $b) use ($epsilon) {
            if (abs($a->getRating() - $b->getRating()) < $epsilon) {
                return 0;
            }

            return $a->getRating() > $b->getRating() ? -1 : 1;
        });

        return new self(array_slice($this->data, 0, $n, true));
    }

    public function containsItem(string $item): bool
    {
        /**
         * @var PredictionInterface $prediction
         */
        foreach ($this as $prediction) {
            if ($prediction->getItem() === $item) {
                return true;
            }
        }

        return false;
    }

    public function getRank(string $item): int
    {
        $index = array_search(
            $item,
            array_map(static function (PredictionInterface $prediction) {
                return $prediction->getItem();
            }, $this->toArray()),
            true
        );

        if ($index === false) {
            return 1;
        }

        return (int)$index + 1;
    }

    public function getCombinations(): Pairs
    {
        $combinations = new Pairs();

        $total = $this->count();
        foreach ($this as $i => $prediction) {
            for ($j = $i + 1; $j < $total; $j++) {
                $combinations->add(new Pair($prediction, $this[$j]));
            }
        }

        return $combinations;
    }
}
