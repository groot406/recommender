<?php declare(strict_types=1);

namespace Recommender\Prediction;

interface PredictionInterface
{
    /**
     * @return string
     */
    public function getUser(): string;

    /**
     * @return string
     */
    public function getItem(): string;

    /**
     * @return float
     */
    public function getActual(): float;

    /**
     * @return float
     */
    public function getRating(): float;

    /**
     * @return float
     */
    public function getError(): float;
}
