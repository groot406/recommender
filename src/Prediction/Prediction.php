<?php declare(strict_types=1);

namespace Recommender\Prediction;

class Prediction implements PredictionInterface
{
    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $item;

    /**
     * @var float
     */
    private $actual;

    /**
     * @var float
     */
    private $rating;

    public function __construct(string $user, string $item, float $rating, float $actual)
    {
        $this->user = $user;
        $this->item = $item;
        $this->rating = $rating;
        $this->actual = $actual;
    }

    public function getUser(): string
    {
        return $this->user;
    }

    public function getItem(): string
    {
        return $this->item;
    }

    public function getRating(): float
    {
        return $this->rating;
    }

    public function getActual(): float
    {
        return $this->actual;
    }

    public function getError(): float
    {
        return abs($this->getActual() - $this->getRating());
    }
}
