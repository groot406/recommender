<?php declare(strict_types=1);

namespace Recommender\Prediction;

use Ramsey\Collection\AbstractCollection;

class Pairs extends AbstractCollection
{
    public function getType(): string
    {
        return Pair::class;
    }
}
