<?php declare(strict_types=1);

namespace Recommender\Recommender;

use Recommender\Item\ItemIdentifier;
use Recommender\Prediction\PredictionInterface;
use Recommender\Prediction\Predictions;
use Recommender\User\UserIdentifier;
use Rubix\ML\Datasets\Dataset;

interface RecommenderAlgorithm
{
    public function fit(Dataset $dataset): void;

    public function test(Dataset $dataset): Predictions;

    public function predict(string $userIdentifier, string $itemIdentifier, ?float $actual): PredictionInterface;
}
