<?php declare(strict_types=1);

namespace Recommender\Recommender;

use Recommender\Prediction\Prediction;
use Recommender\Prediction\PredictionInterface;
use Rubix\ML\Datasets\Dataset;

class RandomRecommender extends ReocmmenderBase
{
    /**
     * @var int
     */
    private $min;

    /**
     * @var int
     */
    private $max;

    public function __construct(int $min, int $max)
    {
        $this->min = $min;
        $this->max = $max;
    }

    public function fit(Dataset $dataset): void
    {
    }

    public function predict(
        string $userIdentifier,
        string $itemIdentifier,
        ?float $actual = null
    ): PredictionInterface {
        $randomRating = (float)random_int($this->min, $this->max);

        if ($actual === null) {
            $actual = $randomRating;
        }

        return new Prediction($userIdentifier, $itemIdentifier, $randomRating, $actual);
    }
}
