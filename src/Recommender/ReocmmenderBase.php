<?php declare(strict_types=1);

namespace Recommender\Recommender;

use Recommender\Prediction\Predictions;
use Rubix\ML\Datasets\Dataset;

abstract class ReocmmenderBase implements RecommenderAlgorithm
{
    public function test(Dataset $dataset): Predictions
    {
        $predictions = new Predictions();
        foreach ($dataset->samples() as $sample) {
            $predictions->add($this->predict($sample[0], $sample[1], $sample[2]));
        }

        return $predictions;
    }
}
