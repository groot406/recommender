<?php declare(strict_types=1);

namespace Recommender\Recommender;

use Recommender\Prediction\Prediction;
use Recommender\Prediction\PredictionInterface;
use Rubix\ML\Datasets\Dataset;

class FixedRecommender extends ReocmmenderBase
{
    /**
     * @var float
     */
    private $fixedValue;

    public function __construct(float $fixedValue)
    {
        $this->fixedValue = $fixedValue;
    }

    public function fit(Dataset $dataset): void
    {
    }

    public function predict(
        string $userIdentifier,
        string $itemIdentifier,
        ?float $actual = null
    ): PredictionInterface {
        if ($actual === null) {
            $actual = $this->fixedValue;
        }

        return new Prediction($userIdentifier, $itemIdentifier, $this->fixedValue, $actual);
    }
}
