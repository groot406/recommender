<?php declare(strict_types=1);

namespace Recommender\Recommender;

use Recommender\Prediction\Prediction;
use Recommender\Prediction\PredictionInterface;
use Recommender\Prediction\Predictions;
use Rubix\ML\Datasets\Dataset;

class GlobalMean implements RecommenderAlgorithm
{
    /**
     * @var array
     */
    private $means = [];

    public function fit(Dataset $dataset): void
    {
        $itemRatings = [];

        foreach ($dataset->samples() as $sample) {
            if (!isset($itemRatings[$sample[1]])) {
                $itemRatings[$sample[1]] = [];
            }
            $itemRatings[$sample[1]][] = $sample[2];
        }

        foreach ($itemRatings as $item => $ratings) {
            $this->means[$item] = array_sum($ratings) / count($ratings);
        }
    }

    public function test(Dataset $dataset): Predictions
    {
        $predictions = new Predictions();
        $samples = $dataset->samples();
        foreach ($samples as $i => $iValue) {
            [$userId, $itemId, $rating] = $samples[$i];

            if (!isset($this->means[$itemId])) {
                continue;
            }

            $predictions->add($this->predict($userId, $itemId, $rating));
        }

        return $predictions;
    }

    public function predict(string $userId, string $itemId, ?float $actual = null): PredictionInterface
    {
        $meanRating = $this->means[$itemId];

        return new Prediction(
            $userId,
            $itemId,
            $meanRating,
            $actual ?? $meanRating
        );
    }
}
