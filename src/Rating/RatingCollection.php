<?php declare(strict_types=1);

namespace Recommender\Rating;

use Ramsey\Collection\AbstractCollection;

class RatingCollection extends AbstractCollection
{
    public function getType(): string
    {
        return RatingInterface::class;
    }
}
