<?php declare(strict_types=1);

namespace Recommender\Rating;

class Rating implements RatingInterface
{
    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $item;

    /**
     * @var float
     */
    private $rating;

    public function __construct(string $user, string $item, float $rating)
    {
        $this->user = $user;
        $this->item = $item;
        $this->rating = $rating;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getItem(): string
    {
        return $this->item;
    }

    /**
     * @return float
     */
    public function getRating(): float
    {
        return $this->rating;
    }
}
