<?php declare(strict_types=1);

namespace Recommender\Rating;

interface RatingRepositoryInterface
{
    public function getRatings(): RatingCollection;
}
