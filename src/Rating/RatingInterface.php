<?php declare(strict_types=1);

namespace Recommender\Rating;

interface RatingInterface
{
    public function getUser(): string;

    public function getItem(): string;

    public function getRating(): float;
}
