<?php declare(strict_types=1);

namespace Recommender\Novelty;

use Recommender\Prediction\Prediction;
use Rubix\ML\Datasets\Dataset;

class RatingCountPopularity implements PopularityScoreInterface
{
    /**
     * @var array|null
     */
    private $ratingCounts;

    /**
     * @var Dataset
     */
    private $dataset;

    public function __construct(Dataset $dataset)
    {
        $this->dataset = $dataset;
    }

    public function getPopularity(Prediction $prediction): float
    {
        return $this->getRatingCounts()[$prediction->getItem()] ?? 0;
    }

    private function getRatingCounts(): array
    {
        if ($this->ratingCounts === null) {
            $this->ratingCounts = [];
            foreach ($this->dataset->samples() as $sample) {
                if (!array_key_exists($sample[1], $this->ratingCounts)) {
                    $this->ratingCounts[$sample[1]] = 1;
                    continue;
                }
                $this->ratingCounts[$sample[1]]++;
            }
        }

        return $this->ratingCounts;
    }
}
