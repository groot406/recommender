<?php declare(strict_types=1);

namespace Recommender\Novelty;

use Recommender\Prediction\Prediction;

interface PopularityScoreInterface
{
    public function getPopularity(Prediction $prediction): float;
}
