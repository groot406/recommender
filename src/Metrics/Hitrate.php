<?php declare(strict_types=1);

namespace Recommender\Metrics;

use Recommender\Prediction\PredictionInterface;
use Recommender\Prediction\Predictions;
use Recommender\User\UserPredictionMap;
use Recommender\User\UserPredictionsMap;

class Hitrate
{
    public function hitrate(
        UserPredictionsMap $topPredictions,
        UserPredictionMap $leftOutPredictions,
        ?callable $scoreModifier = null,
        int $precision = 2
    ): float {
        if ($leftOutPredictions->count() === 0) {
            return 0;
        }

        $hits = 0;
        $total = 0;

        /**
         * @var PredictionInterface $leftOutPrediction
         */
        foreach ($leftOutPredictions as $userId => $leftOutPrediction) {
            $total++;

            if (!$topPredictions->containsKey($userId)) {
                continue;
            }

            /**
             * @var Predictions $userPredictions ;
             */
            $userPredictions = $topPredictions->get($userId);

            if ($userPredictions->containsItem($leftOutPrediction->getItem())) {
                $hitScore = 1;
                if ($scoreModifier !== null) {
                    $hitScore = $scoreModifier($userPredictions, $leftOutPrediction);
                }
                $hits += $hitScore;
            }
        }

        return round($hits / $total, $precision);
    }

    public function cumulativeHitRate(
        UserPredictionsMap $topPredictions,
        UserPredictionMap $leftOutPredictions,
        float $ratingCutoff = 0,
        int $precision = 2
    ): float {
        return $this->hitrate($topPredictions, $leftOutPredictions->ratingAbove($ratingCutoff), null, $precision);
    }

    public function averageReciprocalHitRank(
        UserPredictionsMap $topPredictions,
        UserPredictionMap $leftOutPredictions,
        int $precision = 2
    ): float {
        return $this->hitrate($topPredictions, $leftOutPredictions, [$this, 'reciprocalRank'], $precision);
    }

    /**
     * Returns counts of hits per rating
     * @param UserPredictionsMap $topPredictions
     * @param UserPredictionMap $leftOutPredictions
     * @return array[int, int]
     */
    public function ratingHitRate(UserPredictionsMap $topPredictions, UserPredictionMap $leftOutPredictions): array
    {
        $ratingCounts = [];

        $this->hitrate(
            $topPredictions,
            $leftOutPredictions,
            static function (Predictions $predictions, PredictionInterface $prediction) use (&$ratingCounts) {
                $rating = $prediction->getRating();
                if (!isset($ratingCounts[$rating])) {
                    $ratingCounts[$rating] = 0;
                }
                $ratingCounts[$rating]++;

                return 1;
            }
        );

        return $ratingCounts;
    }

    private function reciprocalRank(Predictions $predictions, PredictionInterface $prediction): float
    {
        $rank = $predictions->getRank($prediction->getItem());

        if ($rank === 0) {
            return 0;
        }

        return 1 / $rank;
    }
}
