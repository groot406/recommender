<?php declare(strict_types=1);

namespace Recommender\Metrics;

use Recommender\Prediction\PredictionInterface;
use Recommender\Prediction\Predictions;

class Accuracy
{
    public function mae(Predictions $predictions, int $precision = 2): float
    {
        return round(
            $this->average(
                array_map(
                    static function (PredictionInterface $prediction) {
                        return $prediction->getError();
                    },
                    $predictions->toArray()
                )
            ),
            $precision
        );
    }

    public function rmse(Predictions $predictions, int $precision = 2): float
    {
        return round(
            sqrt(
                $this->average(
                    array_map(
                        static function (PredictionInterface $prediction) {
                            return $prediction->getError() ** 2;
                        },
                        $predictions->toArray()
                    )
                )
            ),
            $precision
        );
    }

    /**
     * @param array<float> $values
     * @return float
     */
    private function average(array $values): float
    {
        if (count($values) === 0) {
            return 0;
        }
        
        return array_sum($values) / count($values);
    }
}
