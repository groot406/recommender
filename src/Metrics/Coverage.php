<?php declare(strict_types=1);

namespace Recommender\Metrics;

use Recommender\Novelty\PopularityScoreInterface;
use Recommender\Prediction\Pair;
use Recommender\Prediction\Predictions;
use Recommender\Similarity\SimilarityScoreInterface;
use Recommender\User\UserPredictionsMap;

class Coverage
{
    public function userCoverage(Predictions $topPredictions, int $numberOfUsers, float $ratingsThreshold = 0): float
    {
        $predictions = $topPredictions;
        if ($ratingsThreshold > 0) {
            $predictions = $topPredictions->ratingAbove($ratingsThreshold);
        }
        $groupedPredictions = $predictions->groupByUser();

        return count(array_keys($groupedPredictions->toArray())) / $numberOfUsers;
    }

    public function diversity(
        UserPredictionsMap $topPredictions,
        SimilarityScoreInterface $similarityScore
    ): float {
        $totalPairs = 0;
        $totalSimilarity = 0;
        foreach ($topPredictions as $userId => $userPredictions) {
            $pairs = $userPredictions->getCombinations();

            /**
             * @var Pair $pair
             */
            foreach ($pairs as $pair) {
                $totalSimilarity += $similarityScore->getSimilarity($pair);
                $totalPairs++;
            }
        }

        return 1 - ($totalSimilarity / ($totalPairs > 0 ? $totalPairs : 1));
    }

    public function novelty(UserPredictionsMap $topPredictions, PopularityScoreInterface $noveltyScore): float
    {
        $totalNovelty = 0;
        $totalPredictions = 0;
        if (count($topPredictions) === 0) {
            return 0;
        }
        foreach ($topPredictions as $userId => $userPredictions) {
            foreach ($userPredictions as $prediction) {
                $totalNovelty += $noveltyScore->getPopularity($prediction);
                $totalPredictions++;
            }
        }

        return $totalNovelty / $totalPredictions;
    }
}
