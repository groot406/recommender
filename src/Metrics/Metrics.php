<?php declare(strict_types=1);

namespace Recommender\Metrics;

class Metrics
{
    public const MAE = 'mae';
    public const RMSE = 'rmse';
    public const USER_COVERAGE = 'userCoverage';
    public const DIVERSITY = 'diversity';
    public const NOVELTY = 'novelty';
    public const HITRATE = 'hitrate';
    public const CUMULATIVE_HIT_RATE = 'cumulativeHitRate';
    public const AVERAGE_RECIPROCAL_HIT_RANK = 'averageReciprocalHitRank';
    public const RATING_HIT_RATE = 'ratingHitRate';
}
