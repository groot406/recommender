<?php declare(strict_types=1);

namespace Recommender\Similarity;

use Recommender\Prediction\Pair;

interface SimilarityScoreInterface
{
    public function getSimilarity(Pair $pair): float;
}
