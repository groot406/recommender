<?php declare(strict_types=1);

namespace Recommender\Similarity;

use Recommender\Prediction\Pair;

class KnnSimilarity implements SimilarityScoreInterface
{
    public function getSimilarity(Pair $pair): float
    {
        return 1;
    }
}
