<?php declare(strict_types=1);

namespace Recommender\Evaluation;

use PHPUnit\Framework\TestCase;
use Recommender\Recommender\FixedRecommender;
use Recommender\Recommender\GlobalMean;
use Recommender\Recommender\RandomRecommender;
use Rubix\ML\Datasets\Unlabeled;

class EvaluatorTest extends TestCase
{
    public function testEvaluator(): void
    {
        $dataset = new Unlabeled([
            ['1', '1', 1],
            ['1', '2', 3],
            ['1', '3', 5],
            ['1', '4', 5],
            ['1', '5', 5],
            ['1', '6', 5],
            ['2', '1', 1],
            ['2', '2', 1],
            ['2', '3', 5],
            ['2', '4', 1],
            ['3', '1', 3],
            ['3', '2', 5],
            ['3', '3', 5],
            ['3', '1', 4],
            ['3', '8', 2],
            ['4', '4', 5],
            ['4', '7', 3],
            ['4', '44', 1],
            ['4', '12', 1],
            ['4', '11', 5],
            ['5', '10', 5],
            ['6', '15', 1],
            ['5', '11', 5],
            ['6', '12', 1],
        ]);
        $evaluation = new Evaluator($dataset);

        $evaluation->addAlgorithm(new RandomRecommender(1, 5), 'Random (1-5)');
        $evaluation->addAlgorithm(new FixedRecommender(3), 'Fixed (3)');
        $evaluation->addAlgorithm(new GlobalMean(), 'Mean');
        $results = $evaluation->evaluate(null, 10);

        $printer = new ConsoleResultPrinter();
        $printer->print($results);

        // Dummy assertion, we just want the printed results here
        self::assertEquals(1,1);
    }
}
