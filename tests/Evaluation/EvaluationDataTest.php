<?php declare(strict_types=1);

namespace Recommender\Evaluation;

use PHPStan\Testing\TestCase;
use Rubix\ML\Datasets\Unlabeled;

class EvaluationDataTest extends TestCase
{
    public function testConstruct(): void
    {
        $dataset = new Unlabeled([
            ['1', '1', 1],
            ['1', '2', 2],
            ['1', '3', 3],
            ['2', '1', 4],
            ['2', '2', 5],
            ['3', '1', 3]
        ], false);

        //  ITEM    VALUES          MEAN
        //  1       1 + 4 + 3       2.666
        //  2       2 + 5           3.5
        //  3       3               3

        $evaluationDataSet = new EvaluationData($dataset);
        self::assertEquals($dataset, $evaluationDataSet->getFullTrainSet());

        self::assertCount(4, $evaluationDataSet->getTrainSet());
        self::assertCount(2, $evaluationDataSet->getTestSet());
        self::assertCount(3, $evaluationDataSet->getFullAntiTestSet());
        self::assertCount(3, $evaluationDataSet->getLeaveOneOutTrainSet());
        self::assertCount(3, $evaluationDataSet->getLeaveOneOutTestSet());

        self::assertCount(0, $evaluationDataSet->getAntiTestSetForUser('1'));
        self::assertCount(1, $evaluationDataSet->getAntiTestSetForUser('2'));
        self::assertEquals(3, $evaluationDataSet->getAntiTestSetForUser('2')[0][2]);
        self::assertCount(2, $evaluationDataSet->getAntiTestSetForUser('3'));
    }
}
