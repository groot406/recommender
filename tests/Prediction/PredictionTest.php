<?php declare(strict_types=1);

namespace Recommender\Prediction;

use PHPUnit\Framework\TestCase;
use Recommender\User\UserPredictionsMap;

class PredictionTest extends TestCase
{
    public function testTopN():void {
        $predictions = new Predictions([
            new Prediction('1', '1', 1, 1),
            new Prediction('1', '2', 1.2, 1.2),
            new Prediction('1', '3', 3.1, 3.1),
            new Prediction('1', '4', 2.2, 2.2),
            new Prediction('1', '5', 2, 2),

            new Prediction('2', '1', 1, 1),
            new Prediction('2', '2', 1.2, 1.2),
            new Prediction('2', '3', 3.1, 3.1),
            new Prediction('2', '4', 2.2, 2.2),
            new Prediction('2', '5', 2, 2),
        ]);

        $topN = $predictions->groupByUser()->topN(3);
        $this->assertInstanceOf(UserPredictionsMap::class, $topN);
        $this->assertCount(3, $topN['1']);
        $this->assertCount(3, $topN['2']);

        $this->assertEquals(new Prediction('1', '3', 3.1, 3.1), current($topN['1']->toArray()));
        $this->assertEquals(new Prediction('2', '3', 3.1, 3.1), $topN['2']->first());
    }
}
