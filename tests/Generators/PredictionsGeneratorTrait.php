<?php declare(strict_types=1);

namespace Recommender\Generators;

use Recommender\Prediction\Prediction;
use Recommender\Prediction\PredictionInterface;
use Recommender\Prediction\Predictions;

trait PredictionsGeneratorTrait
{
    protected function getPredictions(string $user): Predictions
    {
        return new Predictions([
            $this->prediction(5, 3, $user, 'item1'),
            $this->prediction(4, 1, $user, 'item2'),
            $this->prediction(5, 4, $user, 'item3'),
            $this->prediction(1, 1, $user, 'item4'),
        ]);
    }

    protected function prediction(float $rating, float $actual, string $user, string $item): PredictionInterface
    {
        return new Prediction($user, $item, $rating, $actual);
    }
}
