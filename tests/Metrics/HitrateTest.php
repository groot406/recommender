<?php declare(strict_types=1);

namespace Recommender\Metrics;

use PHPUnit\Framework\TestCase;
use Recommender\Generators\PredictionsGeneratorTrait;
use Recommender\User\UserPredictionMap;

class HitrateTest extends TestCase
{
    use PredictionsGeneratorTrait;

    public function test100PercentHitrate(): void
    {
        $metrics = new Hitrate();
        $hitrate = $metrics->hitrate(
            $this->getPredictions('user1')->merge($this->getPredictions('user2'))->groupByUser()->topN(3),
            new UserPredictionMap([
                'user1' => $this->prediction(5, 5, 'user1', 'item2'),
                'user2' => $this->prediction(1, 1, 'user2', 'item3')
            ])
        );
        self::assertEquals(1, $hitrate);
    }

    public function test0PercentHitrate(): void
    {
        $metrics = new Hitrate();
        $hitrate = $metrics->hitrate(
            $this->getPredictions('user1')->merge($this->getPredictions('user2'))->groupByUser()->topN(3),
            new UserPredictionMap([
                'user1' => $this->prediction(5, 5, 'user1', 'item4'),
                'user2' => $this->prediction(5, 5, 'user2', 'item4'),
            ])
        );
        self::assertEquals(0, $hitrate);
    }

    public function testCumulativeHitrate(): void
    {
        $metrics = new Hitrate();
        $cumulativeHitrate = $metrics->cumulativeHitRate(
            $this->getPredictions('user1')->merge($this->getPredictions('user2'))->groupByUser()->topN(3),
            new UserPredictionMap([
                'user1' => $this->prediction(4, 4, 'user1', 'item1'),
                'user2' => $this->prediction(2, 2, 'user2', 'item3'),
            ]),
            3
        );
        self::assertEquals(1, $cumulativeHitrate);
    }

    public function testAverageReciprocalHitRank(): void
    {
        $metrics = new Hitrate();
        $allPredictions = $this->getPredictions('user1')->merge($this->getPredictions('user2'));
        $top3Predictions = $allPredictions->groupByUser()->topN(3);
        $averageReciprocalHitRank = $metrics->averageReciprocalHitRank(
            $top3Predictions,
            new UserPredictionMap([
                'user1' => $this->prediction(4, 4, 'user1', 'item3'),
                'user2' => $this->prediction(2, 2, 'user2', 'item1'),
            ])
        );
        self::assertEquals(0.67, $averageReciprocalHitRank);
    }

    public function testRatingHitRate(): void
    {
        $metrics = new Hitrate();
        $ratingHitRate = $metrics->ratingHitRate(
            $this->getPredictions('user1')
                ->merge($this->getPredictions('user2'))
                ->merge($this->getPredictions('user3'))
                ->groupByUser()
                ->topN(3),
            new UserPredictionMap([
                'user1' => $this->prediction(4, 4, 'user1', 'item3'),
                'user2' => $this->prediction(2, 2, 'user2', 'item3'),
                'user3' => $this->prediction(2, 1, 'user3', 'item3'),
            ])
        );

        self::assertArrayHasKey(2, $ratingHitRate);
        self::assertEquals(2, $ratingHitRate[2]);
        self::assertArrayHasKey(4, $ratingHitRate);
        self::assertEquals(1, $ratingHitRate[4]);
    }
}
