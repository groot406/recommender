<?php declare(strict_types=1);

namespace Recommender\Metrics;

use PHPUnit\Framework\TestCase;
use Recommender\Generators\PredictionsGeneratorTrait;
use Recommender\Novelty\PopularityScoreInterface;
use Recommender\Prediction\Predictions;
use Recommender\Similarity\SimilarityScoreInterface;

class CoverageTest extends TestCase
{
    use PredictionsGeneratorTrait;

    public function testUserCoverage(): void
    {
        $metrics = new Coverage();

        /**
         * @var Predictions $predictions
         */
        $predictions = $this->getPredictions('user1')
            ->merge($this->getPredictions('user2'))
            ->merge(new Predictions([$this->prediction(2, 3, 'user3', 'item1')]));

        $userCoverage = $metrics->userCoverage($predictions, 4, 4);
        self::assertEquals(0.5, $userCoverage);
    }

    public function testNoDiversity(): void
    {
        $metrics = new Coverage();
        $similarityScore = $this->createMock(SimilarityScoreInterface::class);
        $similarityScore->method('getSimilarity')->willReturn(1.0);
        $diversity = $metrics->diversity($this->getPredictions('user')->groupByUser()->topN(3), $similarityScore);
        self::assertEquals(0, $diversity);
    }

    public function testFullDiversity(): void
    {
        $metrics = new Coverage();
        $similarityScore = $this->createMock(SimilarityScoreInterface::class);
        $similarityScore->method('getSimilarity')->willReturn(0.0);
        $diversity = $metrics->diversity($this->getPredictions('user')->groupByUser()->topN(3), $similarityScore);
        self::assertEquals(1, $diversity);
    }

    public function testNoNovelty(): void
    {
        $metrics = new Coverage();
        $popularityScore = $this->createMock(PopularityScoreInterface::class);
        $popularityScore->method('getPopularity')->willReturn(0.0);
        $novelty = $metrics->novelty($this->getPredictions('user')->groupByUser()->topN(4), $popularityScore);
        self::assertEquals(0, $novelty);
    }

    public function testFullNovelty(): void
    {
        $metrics = new Coverage();
        $popularityScore = $this->createMock(PopularityScoreInterface::class);
        $popularityScore->method('getPopularity')->willReturn(1.0);
        $novelty = $metrics->novelty($this->getPredictions('user')->groupByUser()->topN(4), $popularityScore);
        self::assertEquals(1, $novelty);
    }
}
