<?php declare(strict_types=1);

namespace Recommender\Metrics;

use PHPUnit\Framework\TestCase;
use Recommender\Generators\PredictionsGeneratorTrait;

class AccuracyTest extends TestCase
{
    use PredictionsGeneratorTrait;

    public function testMae(): void
    {
        $metrics = new Accuracy();
        self::assertEquals(1.5, $metrics->mae($this->getPredictions('user1')));
    }

    public function testRmse(): void
    {
        $metrics = new Accuracy();
        self::assertEquals(1.87, $metrics->rmse($this->getPredictions('user1')));
    }
}
